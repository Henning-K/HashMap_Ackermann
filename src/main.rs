use std::cell::RefCell;
use std::collections::HashMap;
use std::time::Instant;

struct Ack {
    values: RefCell<HashMap<(usize, usize), usize>>,
}

impl Ack {
    fn new() -> Self {
        Ack {
            values: RefCell::new(HashMap::new()),
        }
    }

    fn get_or_calc(&self, fst: usize, snd: usize) -> usize {
        if let Some(&x) = self.values.borrow_mut().get(&(fst, snd)) {
            return x;
        }
        let val = self.ack(fst, snd);
        self.values.borrow_mut().insert((fst, snd), val);
        // println!("({}, {}) = {}", fst, snd, val);
        val
    }

    #[inline(always)]
    fn ack(&self, m: usize, n: usize) -> usize {
        if m == 0 {
            n + 1
        } else if n == 0 {
            self.get_or_calc(m - 1, 1)
        } else {
            let snd = self.get_or_calc(m, n - 1);
            self.get_or_calc(m - 1, snd)
        }
    }
}

fn main() {
    let ack = Ack::new();

    for i in 0..5 {
        for j in 0..11 {
            let start = Instant::now();
            let val = ack.ack(i, j);
            let duration = start.elapsed();
            print!("({},{}) = {}, ", i, j, val);
            println!(
                "Time taken: {} seconds",
                duration.as_secs() as f64 + duration.subsec_nanos() as f64 * 1e-9
            );
        }
    }
}

#[test]
fn ack_0_n() {
    let ack: Ack = Ack::new();
    for i in 0..11 {
        let res = ack.ack(0, i);
        assert_eq!(res, i + 1);
    }
}

#[test]
fn ack_1_n() {
    let ack: Ack = Ack::new();
    for i in 0..11 {
        let res = ack.ack(1, i);
        assert_eq!(res, i + 2);
    }
}

#[test]
fn ack_2_n() {
    let ack: Ack = Ack::new();
    for i in 0..11 {
        let res = ack.ack(2, i);
        assert_eq!(res, (i + 1) * 2 + 1);
    }
}

#[test]
fn ack_3_n() {
    let ack: Ack = Ack::new();
    assert_eq!(ack.ack(3, 0), 5);
    assert_eq!(ack.ack(3, 1), 13);
    assert_eq!(ack.ack(3, 2), 29);
    assert_eq!(ack.ack(3, 3), 61);
    assert_eq!(ack.ack(3, 4), 125);
    assert_eq!(ack.ack(3, 5), 253);
    assert_eq!(ack.ack(3, 6), 509);
    assert_eq!(ack.ack(3, 7), 1021);
    assert_eq!(ack.ack(3, 8), 2045);
    assert_eq!(ack.ack(3, 9), 4093);
    assert_eq!(ack.ack(3, 10), 8189);
}

#[test]
fn ack_4_n() {
    let ack: Ack = Ack::new();
    assert_eq!(ack.ack(4, 0), 13);
    assert_eq!(ack.ack(4, 1), 65533);
}
